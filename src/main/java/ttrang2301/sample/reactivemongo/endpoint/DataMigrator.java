/**
 * Copyright (c) 2019 Absolute Software Corporation. All rights reserved. Reproduction or
 * transmission in whole or in part, in any form or by any means, electronic, mechanical or
 * otherwise, is prohibited without the prior written consent of the copyright owner.
 */
package ttrang2301.sample.reactivemongo.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.UUID;

import ttrang2301.sample.reactivemongo.dao.entity.Data;
import ttrang2301.sample.reactivemongo.dao.repository.legacy.LegacyDataRepository;

@RestController
public class DataMigrator {
    
    @Autowired
    private LegacyDataRepository repository;

    @RequestMapping(path = "/legacy/ids", method = RequestMethod.GET)
    public void getAllLegacyIds() {
        repository.findAll()
                .map(Data::getId)
                .subscribe(data -> {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Element ID: " + data);
                });
        repository.findAll().count().map(data -> "COUNT: " + data).subscribe(System.out::print);
    }

    @RequestMapping(path = "/legacy/ids", method = RequestMethod.POST)
    public void create() {
        repository.saveAll(Arrays.asList(
           new Data(UUID.randomUUID().toString()), new Data(UUID.randomUUID().toString()), new Data(UUID.randomUUID().toString()), new Data(UUID.randomUUID().toString())
        )).subscribe();
    }

//    @RequestMapping(path = "/boom", method = RequestMethod.POST)
//    public void boom() {
//
//    }
}

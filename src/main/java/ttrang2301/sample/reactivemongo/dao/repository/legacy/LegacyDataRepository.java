/**
 * Copyright (c) 2019 Absolute Software Corporation. All rights reserved. Reproduction or
 * transmission in whole or in part, in any form or by any means, electronic, mechanical or
 * otherwise, is prohibited without the prior written consent of the copyright owner.
 */
package ttrang2301.sample.reactivemongo.dao.repository.legacy;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import ttrang2301.sample.reactivemongo.dao.entity.Data;

@Repository
public interface LegacyDataRepository extends ReactiveMongoRepository<Data, String> {
}

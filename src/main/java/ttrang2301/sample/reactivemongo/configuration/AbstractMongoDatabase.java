/**
 * Copyright (c) 2019 Absolute Software Corporation. All rights reserved. Reproduction or
 * transmission in whole or in part, in any form or by any means, electronic, mechanical or
 * otherwise, is prohibited without the prior written consent of the copyright owner.
 */
package ttrang2301.sample.reactivemongo.configuration;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import java.util.Collections;

import lombok.Data;

@Data
public abstract class AbstractMongoDatabase {

    protected String host, database, username, password;
    protected int port;

    public MongoDbFactory mongoDbFactory() {
        return new SimpleMongoDbFactory(getMongoClient(), database);
    }

    private MongoClient getMongoClient() {
        return new MongoClient(
                new ServerAddress(host, port),
                MongoCredential.createCredential(username, database, password.toCharArray()),
                MongoClientOptions.builder().build());
    }

    /*
     * Factory method to create the MongoTemplate
     */
    abstract public ReactiveMongoTemplate getMongoTemplate();

}

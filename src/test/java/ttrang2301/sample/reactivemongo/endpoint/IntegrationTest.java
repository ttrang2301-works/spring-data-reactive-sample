/**
 * Copyright (c) 2019 Absolute Software Corporation. All rights reserved. Reproduction or
 * transmission in whole or in part, in any form or by any means, electronic, mechanical or
 * otherwise, is prohibited without the prior written consent of the copyright owner.
 */
package ttrang2301.sample.reactivemongo.endpoint;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;
import ttrang2301.sample.reactivemongo.ReactiveMongoApplication;
import ttrang2301.sample.reactivemongo.dao.entity.Data;
import ttrang2301.sample.reactivemongo.dao.repository.legacy.LegacyDataRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = ReactiveMongoApplication.class)
public class IntegrationTest {

    @Autowired
    LegacyDataRepository repository;

    @Test
    public void test() {
        Flux<Data> flux = repository.findAll();

        StepVerifier.create(flux)
                .assertNext(data -> {
                    System.out.println(data.getId());
                })
                .expectComplete()
                .verify();

    }

}
